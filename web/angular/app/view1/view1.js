'use strict';

angular.module('homework.view1', ['homework.utils', 'ngRoute'])

.config(function($routeProvider, fixAssetUrl) {
  $routeProvider.when('/view1', {
    templateUrl: fixAssetUrl('view1/view1.html'),
    controller: 'View1Ctrl'
  });
})

.controller('View1Ctrl', function($scope) {
  $scope.var = 'It works';
});