'use strict';

describe('homework.view1 module', function() {

  beforeEach(module('homework.view1'));

  describe('view1 controller', function(){

    it('should have var', inject(function($controller) {
      var $scope = {};
      var view1Ctrl = $controller('View1Ctrl', {$scope: $scope});
      expect($scope.var).toBe('It works');
    }));

  });
});