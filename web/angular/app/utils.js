'use strict';

angular.module('homework.utils', []).

constant('fixAssetUrl', function (url) {
    return 'angular/app/' + url;
});