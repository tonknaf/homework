<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\TestStatus;

class TestStatusTest extends \PHPUnit_Framework_TestCase {

    public function test_it_throws_invalid() {

        $this->expectException(\InvalidArgumentException::class);
        new TestStatus(10);
    }
}