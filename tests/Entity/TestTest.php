<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\NoQuestionException;
use AppBundle\Entity\RightAnswerException;
use AppBundle\Entity\Test;
use AppBundle\Entity\TestIsEndedException;
use AppBundle\Entity\TestStatus;
use AppBundle\Entity\UnansweredQuestionException;
use AppBundle\Entity\UsedWordException;
use AppBundle\Entity\Word;
use AppBundle\Entity\WrongAnswerException;
use AppBundle\Value\Username;
use Doctrine\Common\Collections\ArrayCollection;

class TestTest extends \PHPUnit_Framework_TestCase {

    public function test_it_begins() {

        $name = new Username('username');
        $test = Test::begin($name);

        $this->assertEquals($name, $test->getName());
        $this->assertTrue($test->getStatus()->equalTo(TestStatus::NoQuestion()));
        $this->assertEquals(0, $test->getScore());
        $this->assertEquals(3, $test->getMistakesAvailable());
        $this->assertFalse($test->hasUnansweredQuestion());
        $this->assertInstanceOf(ArrayCollection::class, $test->getQuestions());

        return $test;
    }

    /**
     * @depends test_it_begins
     */
    public function test_impossible_to_ask_new_question_if_there_is_unanswered_question(Test $test) {

        $test->askNewQuestion(Word::recordRussianWord('волк'),
            Word::recordEnglishWord('wolf'), [
                Word::recordEnglishWord('sky'),
                Word::recordEnglishWord('tree'),
                Word::recordEnglishWord('water')
            ]);
        $this->expectException(UnansweredQuestionException::class);
        $test->askNewQuestion(Word::recordRussianWord('волк'),
            Word::recordEnglishWord('wolf'), [
                Word::recordEnglishWord('sky'),
                Word::recordEnglishWord('tree'),
                Word::recordEnglishWord('water')
            ]);
    }

    public function test_ended() {

        $test = new Test();

        $test->end();

        $this->assertTrue($test->getStatus()->equalTo(TestStatus::Ended()));

        return $test;
    }

    /**
     * @depends test_ended
     */
    public function test_impossible_to_ask_new_question_if_it_is_ended(Test $test) {

        $this->expectException(TestIsEndedException::class);
        $test->askNewQuestion(Word::recordRussianWord('волк'),
            Word::recordEnglishWord('wolf'), [
                Word::recordEnglishWord('sky'),
                Word::recordEnglishWord('tree'),
                Word::recordEnglishWord('water')
            ]);
    }

    public function test_impossible_to_answer_on_test_without_question() {

        $test = Test::begin(new Username('Neo'));
        $this->expectException(NoQuestionException::class);
        $test->answer(Word::recordRussianWord('test'));
    }

    public function test_it_throws_right_answer_exception_and_increases_score() {

        $test = Test::begin(new Username('Neo'));

        $questionWord = Word::recordRussianWord('волк');
        $answer = Word::recordEnglishWord('wolf');

        $test->askNewQuestion($questionWord, $answer, [
            Word::recordEnglishWord('sky'),
            Word::recordEnglishWord('tree'),
            Word::recordEnglishWord('water')
        ]);

        try {
            $test->answer($answer);
            $this->fail('Right answer was not thrown.');
        } catch (RightAnswerException $e) {
        }
        $this->assertEquals(1, $test->getScore());
        $this->assertTrue(TestStatus::NoQuestion()->equalTo($test->getStatus()));
        

        return [$test, $questionWord, $answer];
    }

    /**
     * @depends test_it_throws_right_answer_exception_and_increases_score
     */
    public function test_impossible_to_ask_new_question_with_used_word(array $data) {

        list($test, $questionWord, $answer) = $data;

        try {
            $test->askNewQuestion($questionWord, $answer, [
                Word::recordEnglishWord('sky'),
                Word::recordEnglishWord('tree'),
                Word::recordEnglishWord('water')
            ]);
            $this->fail('Exception was not thrown.');
        } catch (UsedWordException $e) {

        }

        return $test;
    }

    public function test_after_3_wrong_answers_test_will_end() {

        $wrongAnswers = [
            Word::recordRussianWord('аблоко'),
            Word::recordRussianWord('человек'),
            Word::recordRussianWord('змея'),
        ];

        $test = Test::begin(new Username('Neo'));

        $test->askNewQuestion(Word::recordEnglishWord('cake'),
            Word::recordRussianWord('торт'), $wrongAnswers);

        try {
            $test->answer($wrongAnswers[0]);
            $this->fail('Exception was not thrown.');
        } catch (WrongAnswerException $e) {
        }

        $this->assertEquals(2, $test->getMistakesAvailable());

        try {
            $test->answer($wrongAnswers[1]);
            $this->fail('Exception was not thrown.');
        } catch (WrongAnswerException $e) {
        }

        $this->assertEquals(1, $test->getMistakesAvailable());

        try {
            $test->answer($wrongAnswers[2]);
            $this->fail('Exception not thrown.');
        } catch (TestIsEndedException $e) {
        }

        $this->assertEquals(0, $test->getMistakesAvailable());
        $this->assertTrue($test->isEnded());
    }
}