<?php

namespace Tests\AppBundle\DTO;

use AppBundle\DTO\Test as TestDTO;
use AppBundle\DTO\TestDTOConverter;
use AppBundle\Entity\Test;
use AppBundle\Entity\TestStatus;
use AppBundle\Entity\Word;
use Doctrine\Common\Collections\ArrayCollection;

class TestDTOConverterTest extends \PHPUnit_Framework_TestCase {

    public function test_converting_test_without_question() {

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('getId')->willReturn(1);
        $test->expects($this->once())->method('getStatus')->willReturn(TestStatus::NoQuestion());
        $test->expects($this->once())->method('getScore')->willReturn(3);
        $test->expects($this->once())->method('getName')->willReturn('Morpheus');
        $test->expects($this->once())->method('getMistakesAvailable')->willReturn(3);
        $test->expects($this->exactly(2))->method('hasUnansweredQuestion')->willReturn(false);


        $converter = new TestDTOConverter();

        $dto = $converter->convert($test);

        $this->assertInstanceOf(TestDTO::class, $dto);
        $this->assertEquals(1, $dto->id);
        $this->assertEquals(TestStatus::NoQuestion()->toNative(), $dto->status);
        $this->assertEquals(3, $dto->score);
        $this->assertEquals('Morpheus', $dto->name);
        $this->assertEquals(3, $dto->mistakesAvailable);
        $this->assertNull($dto->question);
        $this->assertNull($dto->answers);
    }

    public function test_converting_test_with_question() {

        $drafts = [
            [1, 'apple', true],
            [2, 'banana', false],
            [3, 'orange', true],
            [4, 'juice', false]
        ];
        $answers = new ArrayCollection();
        foreach ($drafts as $draft) {
            list($id, $word) = $draft;

            $answers->add($answer = $this->createMock(Word::class));
            $answer->expects($this->once())->method('getId')->willReturn($id);
            $answer->expects($this->once())->method('getWord')->willReturn($word);
        }

        $question = $this->createMock(Word::class);
        $question->expects($this->once())->method('getWord')->willReturn('яблоко');

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('getId')->willReturn(1);
        $test->expects($this->once())->method('getStatus')->willReturn(TestStatus::UnansweredQuestion());
        $test->expects($this->once())->method('getScore')->willReturn(3);
        $test->expects($this->once())->method('getName')->willReturn('Morpheus');
        $test->expects($this->once())->method('getMistakesAvailable')->willReturn(3);
        $test->expects($this->exactly(2))->method('hasUnansweredQuestion')->willReturn(true);
        $test->expects($this->once())->method('getQuestion')->willReturn($question);
        $test->expects($this->once())->method('getAnswers')->willReturn($answers);
        $test->expects($this->exactly(4))->method('getIsFailedAnswer')->withConsecutive($answers[0], $answers[1],
            $answers[2], $answers[3])->willReturnCallback(function ($answer) use ($answers, $drafts) {

            return $drafts[$answers->indexOf($answer)][2];
        });

        $converter = new TestDTOConverter();

        $dto = $converter->convert($test);

        $this->assertInstanceOf(TestDTO::class, $dto);
        $this->assertEquals(1, $dto->id);
        $this->assertEquals(TestStatus::UnansweredQuestion()->toNative(), $dto->status);
        $this->assertEquals(3, $dto->score);
        $this->assertEquals('Morpheus', $dto->name);
        $this->assertEquals(3, $dto->mistakesAvailable);
        $this->assertEquals('яблоко', $dto->question);

    }
}
