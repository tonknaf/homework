<?php

namespace Tests\Infrastructure\Entity;

use AppBundle\Entity\Test;
use AppBundle\Entity\TestStatus;
use AppBundle\Entity\Word;
use AppBundle\Entity\WrongAnswerException;
use AppBundle\Value\Username;
use Tests\DBTestCase;

class TestTest extends DBTestCase {
    
    public function test_it_saves_relations()
    {
        $wrongAnswers = [];
        $entities = [
            $question = Word::recordRussianWord('слон'),
            $rightAnswer = Word::recordEnglishWord('elephant'),
            $wrongAnswers[] = Word::recordEnglishWord('chair'),
            $wrongAnswers[] = Word::recordEnglishWord('moon'),
            $wrongAnswers[] = Word::recordEnglishWord('spear'),
            $test = Test::begin(new Username('Neo'))
        ];

        $test->askNewQuestion($question, $rightAnswer, $wrongAnswers);

        foreach ($entities as $e) {
            $this->em->persist($e);
        }
        $this->em->flush();
        $this->em->clear();

        $repository = $this->em->getRepository(Test::class);
        $wordRepository = $this->em->getRepository(Word::class);

        /**
         * @var Test $test
         */
        $test = $repository->find($test->getId());

        $this->assertInstanceOf(Test::class, $test);

        $this->assertTrue($test->getStatus()->equalTo(TestStatus::UnansweredQuestion()));
        $this->assertEquals(0, $test->getScore());
        $this->assertEquals('Neo', $test->getName());
        $this->assertInstanceOf(Word::class, $test->getQuestion());
        $this->assertEquals('слон', $test->getQuestion()->getWord());
        $this->assertCount(4, $test->getAnswers()); // assert it saves answers
        $this->assertCount(1, $test->getQuestions()); // assert it saves questions collection

        try {
            $test->answer($wordRepository->find($wrongAnswers[0]->getId()));
        } catch (WrongAnswerException $e) {}
        $this->em->persist($test);
        $this->em->flush();
        $this->em->clear();

        /**
         * @var Test $test
         */
        $test = $repository->find($test->getId());

        // assert it saves failed questions
        $this->assertTrue($test->getIsFailedAnswer($wordRepository->find($wrongAnswers[0]->getId())));
    }
}