<?php

namespace Tests\Infrastructure\Repository;

use AppBundle\Entity\Test;
use AppBundle\Value\Username;
use Tests\DBTestCase;

class TestRepositoryTest extends DBTestCase {

    public function test_find()
    {
        $test = Test::begin(new Username('Vasily'));

        $this->em->persist($test);
        $this->em->flush();

        $this->em->detach($test);

        $repository = static::$kernel->getContainer()->get('app.service.test_repository');
        $test = $repository->find($test->getId());

        $this->assertNotNull($test);
        $this->assertEquals('Vasily', $test->getName());
    }
}