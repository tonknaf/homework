<?php

namespace Tests\Acceptance;

use AppBundle\DataFixtures\ORM\LoadDictionaryData;
use AppBundle\Entity\Test;
use AppBundle\Entity\Word;
use AppBundle\Value\Username;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TestAPITest extends WebTestCase {

    public function test_get() {
        $test = Test::begin(new Username('vasya'));
        $this->em->persist($test);
        $this->em->flush();

        $client = $this->createClient();
        $client->request('GET', '/test/'.$test->getId());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function test_begin()
    {
        $client = $this->createClient();
        $client->request('POST', '/test/', [
            'name' => 'Man'
        ]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function test_answer()
    {
        $question = Word::recordEnglishWord('car');
        $rightAnswer = Word::recordRussianWord('машина');
        $wrongAnswers = [
            Word::recordRussianWord('телевизор'),
            Word::recordRussianWord('астрология'),
            Word::recordRussianWord('мрамор')
        ];

        $test = Test::begin(new Username('vasya'));
        $test->askNewQuestion($question, $rightAnswer, $wrongAnswers);
        $this->em->persist($test);
        $this->em->flush();

        $client = $this->createClient();
        $client->request('PUT', '/test/'. $test->getId(), [
            'wordId' => $rightAnswer->getId()
        ]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testAsk()
    {
        $fixtureLoader = new ContainerAwareLoader(self::$kernel->getContainer());
        $fixtureLoader->addFixture(new LoadDictionaryData());

        $fixtureExecutor = new ORMExecutor($this->em, new ORMPurger($this->em));
        $fixtureExecutor->execute($fixtureLoader->getFixtures());

        $test = Test::begin(new Username('vasya'));
        $this->em->persist($test);
        $this->em->flush();

        $client = $this->createClient();
        $client->request('POST', '/test/'. $test->getId());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
