<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Application\AnswerQuestion;
use AppBundle\Application\AskNewQuestion;
use AppBundle\Application\BeginTest;
use AppBundle\Application\GetTest;
use AppBundle\Application\InvalidUsernameException;
use AppBundle\Application\RightAnswerException;
use AppBundle\Application\TestIsEndedException;
use AppBundle\Application\TestNotFoundException;
use AppBundle\Application\UnansweredQuestionException;
use AppBundle\Application\WordNotFoundException;
use AppBundle\Application\WrongAnswerException;
use AppBundle\Controller\TestController;
use AppBundle\DTO\Test as TestDTO;
use AppBundle\Entity\TestStatus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TestControllerTest extends \PHPUnit_Framework_TestCase {

    public function test_beginAction_returns_invalid_response_if_no_name_provided()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('name')->willReturn(null);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->beginAction($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_JUST_ERROR, $this->getResponseContent($response)->code);
    }

    public function test_beginAction_returns_invalid_response_if_name_is_not_string()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('name')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->beginAction($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_JUST_ERROR, $this->getResponseContent($response)->code);
    }

    public function test_beginAction_returns_invalid_response_if_name_invalid()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $beginTest->expects($this->once())->method('execute')->with('(*&#')->willThrowException(
            new InvalidUsernameException()
        );
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('name')->willReturn('(*&#');

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->beginAction($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_JUST_ERROR, $this->getResponseContent($response)->code);
    }

    public function test_beginAction_returns_id_of_created_test()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $beginTest->expects($this->once())->method('execute')->with('Morpheus')->willReturn(9);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('name')->willReturn('Morpheus');

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->beginAction($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(9, $this->getResponseContent($response)->data->id);
    }

    public function test_getAction_returns_404()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $getTest->expects($this->once())->method('execute')->with(10)->willThrowException(new TestNotFoundException);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->getAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_JUST_ERROR, $this->getResponseContent($response)->code);
    }

    public function test_getAction_returns_dto()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $getTest->expects($this->once())->method('execute')->with(10)->willReturn(
            new TestDTO(
                10,
                TestStatus::NoQuestion()->toNative(),
                5,
                'Vasiliy',
                3
            )
        );
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->getAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(10, json_decode($response->getContent())->data->id);
    }

    public function test_askAction_returns_ok()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $askNewQuestion->expects($this->once())->method('execute')->with(10);
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->askAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
    }

    public function test_askAction_returns_404()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $askNewQuestion->expects($this->once())->method('execute')->with(10)->willThrowException(new TestNotFoundException());
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->askAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_askAction_returns_test_is_ended_error()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $askNewQuestion->expects($this->once())->method('execute')->with(10)->willThrowException(new TestIsEndedException());
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->askAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_TEST_IS_ENDED, json_decode($response->getContent())->code);
    }

    public function test_askAction_returns_unanswered_question_error()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $askNewQuestion->expects($this->once())->method('execute')->with(10)->willThrowException(new UnansweredQuestionException());
        $answerQuestion = $this->createMock(AnswerQuestion::class);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->askAction(10);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_UNANSWERED_QUESTION, json_decode($response->getContent())->code);
    }

    public function test_answerAction_return_invalid_when_no_wordId_provided()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(null);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    public function test_answerAction_return_invalid_if_wordId_is_not_integer()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn('adsf');

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    public function test_answerAction_return_404_if_test_not_found()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $answerQuestion->expects($this->once())->method('execute')->willThrowException(new TestNotFoundException());
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_answerAction_return_404_if_word_not_found()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $answerQuestion->expects($this->once())->method('execute')->willThrowException(new WordNotFoundException());
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_answerAction_return_ok()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $answerQuestion->expects($this->once())->method('execute')->willThrowException(new RightAnswerException());
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
    }

    public function test_answerAction_return_wrong_answer()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $answerQuestion->expects($this->once())->method('execute')->willThrowException(new WrongAnswerException());
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_WRONG_ANSWER, $this->getResponseContent($response)->code);
    }

    public function test_answerAction_return_test_is_ended()
    {
        $beginTest = $this->createMock(BeginTest::class);
        $getTest = $this->createMock(GetTest::class);
        $askNewQuestion = $this->createMock(AskNewQuestion::class);
        $answerQuestion = $this->createMock(AnswerQuestion::class);
        $answerQuestion->expects($this->once())->method('execute')->willThrowException(new TestIsEndedException());
        $request = $this->createMock(Request::class);
        $request->expects($this->once())->method('get')->with('wordId')->willReturn(10);

        $controller = new TestController(
            $beginTest,
            $getTest,
            $askNewQuestion,
            $answerQuestion
        );

        $response = $controller->answerAction($request, 5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertEquals(TestController::CODE_TEST_IS_ENDED, $this->getResponseContent($response)->code);
    }

    private function getResponseContent(JsonResponse $response)
    {
        return json_decode($response->getContent());
    }
}