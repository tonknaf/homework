<?php

namespace Tests\AppBundle\Application;

use AppBundle\Application\AnswerQuestion;
use AppBundle\Application\NoQuestionException;
use AppBundle\Application\RightAnswerException;
use AppBundle\Application\TestIsEndedException;
use AppBundle\Application\TestNotFoundException;
use AppBundle\Application\WordNotFoundException;
use AppBundle\Application\WrongAnswerException;
use AppBundle\Entity\Test;
use AppBundle\Entity\Word;
use AppBundle\Repository\TestRepository;
use AppBundle\Repository\WordRepository;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity as DomainExceptions;

class AnswerQuestionTest extends \PHPUnit_Framework_TestCase {

    public function test_it_throws_test_not_found() {

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn(null);
        $wordRepository = $this->createMock(WordRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(TestNotFoundException::class);
        $command->execute(1, 2);
    }

    public function test_it_throws_word_not_found() {

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($this->createMock(Test::class));
        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn(null);
        $em = $this->createMock(EntityManager::class);

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(WordNotFoundException::class);
        $command->execute(1, 2);
    }

    public function test_right_answer() {

        $word = $this->createMock(Word::class);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('answer')->with($word)->willThrowException(new DomainExceptions\RightAnswerException());

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn($word);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())->method('persist')->with($test);
        $em->expects($this->once())->method('flush');

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(RightAnswerException::class);
        $command->execute(1, 2);
    }

    public function test_wrong_answer() {

        $word = $this->createMock(Word::class);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('answer')->with($word)->willThrowException(new DomainExceptions\WrongAnswerException());
        $test->expects($this->once())->method('isEnded')->willReturn(false);

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn($word);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())->method('persist')->with($test);
        $em->expects($this->once())->method('flush');

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(WrongAnswerException::class);
        $command->execute(1, 2);
    }

    public function test_wrong_answer_ending_test() {

        $word = $this->createMock(Word::class);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('answer')->with($word)->willThrowException(new DomainExceptions\WrongAnswerException());
        $test->expects($this->once())->method('isEnded')->willReturn(true);

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn($word);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())->method('persist')->with($test);
        $em->expects($this->once())->method('flush');

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(TestIsEndedException::class);
        $command->execute(1, 2);
    }

    public function test_no_question() {

        $word = $this->createMock(Word::class);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('answer')->with($word)->willThrowException(new DomainExceptions\NoQuestionException());

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn($word);

        $em = $this->createMock(EntityManager::class);

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(NoQuestionException::class);
        $command->execute(1, 2);
    }

    public function test_answer_does_not_belong_converts_to_word_not_found() {

        $word = $this->createMock(Word::class);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('answer')->with($word)->willThrowException(new DomainExceptions\AnswerDoesNotBelongToTheQuestionException());

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('find')->with(2)->willReturn($word);

        $em = $this->createMock(EntityManager::class);

        $command = new AnswerQuestion($testRepository, $wordRepository, $em);

        $this->expectException(WordNotFoundException::class);
        $command->execute(1, 2);
    }
}