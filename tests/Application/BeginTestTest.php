<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Application\BeginTest;
use AppBundle\Application\InvalidUsernameException;
use AppBundle\Entity\Test;
use Doctrine\ORM\EntityManager;

class BeginTestTest extends \PHPUnit_Framework_TestCase {

    public function test_it_throws_invalid_username() {

        $em = $this->createMock(EntityManager::class);

        $beginTest = new BeginTest($em);

        $this->expectException(InvalidUsernameException::class);
        $beginTest->execute('!(%*&');
    }

    public function test_it_begins_test() {

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())->method('persist')->with($this->callback(function (Test $test) {

            $test->_setId(1);
            return $test->getName() === 'Richard Branson';
        }));
        $em->expects($this->once())->method('flush');

        $beginTest = new BeginTest($em);

        $result = $beginTest->execute('Richard Branson');

        $this->assertEquals(1, $result);
    }
}