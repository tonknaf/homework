<?php

namespace Tests\AppBundle\Application;

use AppBundle\Application\AskNewQuestion;
use AppBundle\Application\TestIsEndedException;
use AppBundle\Application\TestNotFoundException;
use AppBundle\Application\UnansweredQuestionException;
use AppBundle\Entity\Test;
use AppBundle\Entity\Translation;
use AppBundle\Entity\Word;
use AppBundle\Repository\TestRepository;
use AppBundle\Repository\TranslationRepository;
use AppBundle\Repository\WordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;

class BeginTestTest extends \PHPUnit_Framework_TestCase {

    public function test_it_throws_test_not_found_exception() {

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn(null);
        $wordRepository = $this->createMock(WordRepository::class);
        $translationRepository = $this->createMock(TranslationRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $this->expectException(TestNotFoundException::class);
        $command->execute(1);
    }

    public function test_it_throws_test_test_is_ended() {

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('isEnded')->willReturn(true);

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);
        $wordRepository = $this->createMock(WordRepository::class);
        $translationRepository = $this->createMock(TranslationRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $this->expectException(TestIsEndedException::class);
        $command->execute(1);
    }

    public function test_it_throws_unanswered_question() {

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('isEnded')->willReturn(false);
        $test->expects($this->once())->method('hasUnansweredQuestion')->willReturn(true);

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);
        $wordRepository = $this->createMock(WordRepository::class);
        $translationRepository = $this->createMock(TranslationRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $this->expectException(UnansweredQuestionException::class);
        $command->execute(1);
    }

    public function test_it_throws_exception_if_no_words_in_database() {

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('isEnded')->willReturn(false);
        $test->expects($this->once())->method('hasUnansweredQuestion')->willReturn(false);
        $test->expects($this->once())->method('getUsedWords')->willReturn(new ArrayCollection());

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);
        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('findRandomWord')->willThrowException(new NoResultException());

        $translationRepository = $this->createMock(TranslationRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $this->expectException(\Exception::class);
        $command->execute(1);
    }

    public function test_it_throws_exception_if_no_more_unused_words() {

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('isEnded')->willReturn(false);
        $test->expects($this->once())->method('hasUnansweredQuestion')->willReturn(false);
        $usedWord = $this->createMock(Word::class);
        $usedWord->expects($this->once())->method('getId')->willReturn(10);

        $test->expects($this->once())->method('getUsedWords')->willReturn(new ArrayCollection([
            $usedWord
        ]));

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);
        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->once())->method('findRandomWord')->willThrowException(new NoResultException());

        $translationRepository = $this->createMock(TranslationRepository::class);
        $em = $this->createMock(EntityManager::class);

        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $this->expectException(TestIsEndedException::class);
        $command->execute(1);
    }

    public function test_it_works() {

        $word1 = $this->createMock(Word::class);
        $word1->expects($this->once())->method('getId')->willReturn(1);

        $word2 = $this->createMock(Word::class);
        $word2->expects($this->once())->method('getId')->willReturn(2);

        $test = $this->createMock(Test::class);
        $test->expects($this->once())->method('isEnded')->willReturn(false);
        $test->expects($this->once())->method('hasUnansweredQuestion')->willReturn(false);
        $test->expects($this->once())->method('getUsedWords')->willReturn(new ArrayCollection([
            $word1,
            $word2
        ]));

        $question = $this->createMock(Word::class);
        $question->expects($this->once())->method('getId')->willReturn(4);

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $wrongAnswers = [
            $this->createMock(Word::class),
            $this->createMock(Word::class),
            $this->createMock(Word::class)
        ];
        $wordRepository = $this->createMock(WordRepository::class);
        $wordRepository->expects($this->exactly(2))->method('findRandomWord')->withConsecutive([[1, 2]],
            [[5], Word::LANG_RU, 3])->willReturnOnConsecutiveCalls($question, $wrongAnswers);

        $rightAnswer = $this->createMock(Word::class);
        $rightAnswer->expects($this->once())->method('getId')->willReturn(5);
        $rightAnswer->expects($this->once())->method('getLang')->willReturn(Word::LANG_RU);

        $translation = $this->createMock(Translation::class);
        $translation->expects($this->once())->method('getTranslation')->with($question)->willReturn($rightAnswer);

        $translationRepository = $this->createMock(TranslationRepository::class);
        $translationRepository->expects($this->once())->method('findTranslationForWord')->with(4)->willReturn($translation);

        $test->expects($this->once())->method('askNewQuestion')->with($question, $rightAnswer, $wrongAnswers);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())->method('persist')->with($test);
        $em->expects($this->once())->method('flush');
        $command = new AskNewQuestion($testRepository, $wordRepository, $translationRepository, $em);

        $command->execute(1);
    }
}