<?php

namespace Tests\AppBundle\Application;

use AppBundle\Application\GetTest;
use AppBundle\Application\TestNotFoundException;
use AppBundle\DTO\Test as TestDTO;
use AppBundle\DTO\TestDTOConverter;
use AppBundle\Entity\Test;
use AppBundle\Repository\TestRepository;

class GetTestTest extends \PHPUnit_Framework_TestCase {

    public function test_it_throws_not_found() {

        $repository = $this->createMock(TestRepository::class);
        $repository->expects($this->once())->method('find')->with(1)->willReturn(null);

        $getTest = new GetTest($repository, $this->createMock(TestDTOConverter::class));

        $this->expectException(TestNotFoundException::class);
        $getTest->execute(1);
    }

    public function test_it_returns_dto() {

        $test = $this->createMock(Test::class);

        $repository = $this->createMock(TestRepository::class);
        $repository->expects($this->once())->method('find')->with(1)->willReturn($test);

        $dto = $this->createMock(TestDTO::class);

        $converter = $this->createMock(TestDTOConverter::class);
        $converter->expects($this->once())->method('convert')->with($test)->willReturn($dto);

        $getTest = new GetTest($repository, $converter);

        $result = $getTest->execute(1);

        $this->assertEquals($dto, $result);
    }
}