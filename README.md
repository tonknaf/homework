## Homework

![Code coverage](/coverage.png)

###  Installation
	$ git clone https://bitbucket.org/tonknaf/homework
	$ composer install
	$ ( cd web/angular ; npm install )
### Run server
	$ php bin/console server:run
### Run backend tests
	$ ./vendor/bin/phpunit

### Run angular unit tests
	$ ( cd web/angular ; npm test )
### Run angular e2e tests (php server must be running)
   	$ cd web/angular
	$ npm run update-webdriver # once
	$ npm run protractor