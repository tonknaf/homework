<?php

namespace AppBundle\Controller;

use AppBundle\Application\AnswerQuestion;
use AppBundle\Application\AskNewQuestion;
use AppBundle\Application\BeginTest;
use AppBundle\Application\GetTest;
use AppBundle\Application\InvalidUsernameException;
use AppBundle\Application\NoQuestionException;
use AppBundle\Application\RightAnswerException;
use AppBundle\Application\TestNotFoundException;
use AppBundle\Application\UnansweredQuestionException;
use AppBundle\Application\WordNotFoundException;
use AppBundle\Application\WrongAnswerException;
use AppBundle\Application\TestIsEndedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController {

    const CODE_JUST_ERROR = 0;
    const CODE_WRONG_ANSWER = 1;
    const CODE_NO_QUESTION = 2;
    const CODE_TEST_IS_ENDED = 3;
    const CODE_UNANSWERED_QUESTION = 4;

    /**
     * @var BeginTest
     */
    private $beginTest;

    /**
     * @var GetTest
     */
    private $getTest;

    /**
     * @var AskNewQuestion
     */
    private $askNewQuestion;

    /**
     * @var AnswerQuestion
     */
    private $answerQuestion;

    public function __construct(
        BeginTest $beginTest,
        GetTest $getTest,
        AskNewQuestion $askNewQuestion,
        AnswerQuestion $answerQuestion
    ) {

        $this->beginTest = $beginTest;
        $this->getTest = $getTest;
        $this->askNewQuestion = $askNewQuestion;
        $this->answerQuestion = $answerQuestion;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function beginAction(Request $request) {
        $name = $request->get('name');

        if ( ! $name || ! is_string($name)) {
            return $this->sendErrorResponse('Name should be a string.');
        }

        try {
            return $this->sendDataResponse([
                'id' => $this->beginTest->execute($name)
            ]);
        } catch (InvalidUsernameException $e) {
            return $this->sendErrorResponse('Invalid name format.');
        }
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function getAction(int $id) {
        try {
            return $this->sendDataResponse(
                $this->getTest->execute($id)
            );
        } catch (TestNotFoundException $e) {
            return $this->sendTestNotFoundResponse();
        }
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function askAction(int $id) {
        try {
            $this->askNewQuestion->execute($id);
            return $this->sendOk();
        } catch (TestNotFoundException $e) {
            return $this->sendTestNotFoundResponse();
        } catch (TestIsEndedException $e) {
            return $this->sendTestIsEndedResponse();
        } catch (UnansweredQuestionException $e) {
            return $this->sendErrorResponse('There is unanswered question.', self::CODE_UNANSWERED_QUESTION);
        }
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     * @throws \App\Application\RepeatingAnswerException
     * @throws \Exception
     */
    public function answerAction(Request $request, $id) {
        $wordId = $request->get('wordId');

        if (!$wordId || !is_int($wordId)) {
            return $this->sendErrorResponse('Invalid word id.');
        }

        try {
            $this->answerQuestion->execute($id, $wordId);
        } catch (TestNotFoundException $e) {
            return $this->sendTestNotFoundResponse();
        } catch (WordNotFoundException $e) {
            return $this->sendErrorResponse('Answer not found.', self::CODE_JUST_ERROR, JsonResponse::HTTP_NOT_FOUND);
        } catch (RightAnswerException $e) {
            return $this->sendOk();
        } catch (WrongAnswerException $e) {
            return $this->sendErrorResponse('Answer is wrong.', self::CODE_WRONG_ANSWER);
        } catch (NoQuestionException $e) {
            return $this->sendErrorResponse('No question to answer.', self::CODE_NO_QUESTION);
        } catch (TestIsEndedException $e) {
            return $this->sendTestIsEndedResponse();
        }

        throw new \Exception('Exception should be thrown.');
    }

    /**
     * @return JsonResponse
     */
    private function sendTestNotFoundResponse()
    {
        return $this->sendErrorResponse(
            'Test not found.',
            self::CODE_JUST_ERROR,
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @return JsonResponse
     */
    private function sendTestIsEndedResponse()
    {
        return $this->sendErrorResponse(
            'Test is ended.',
            self::CODE_TEST_IS_ENDED
        );
    }

    /**
     * @param $message
     * @param int $code
     * @param int $httpCode
     * @param array $data
     *
     * @return JsonResponse
     */
    private function sendErrorResponse(
        $message,
        $code = self::CODE_JUST_ERROR,
        $httpCode = Response::HTTP_UNPROCESSABLE_ENTITY,
        $data = []
    ) {

        return $this->sendResponse([
            'message' => $message,
            'code' => $code,
            'data' => $data
        ], $httpCode);
    }

    /**
     * @param $data
     *
     * @return JsonResponse
     */
    private function sendDataResponse($data) {
        return $this->sendResponse([
            'data' => $data
        ]);
    }

    /**
     * @return JsonResponse
     */
    private function sendOk() {
        return $this->sendResponse();
    }

    /**
     * @param array $data
     * @param int $httpCode
     *
     * @return JsonResponse
     */
    private function sendResponse($data = [], $httpCode = Response::HTTP_OK) {

        return JsonResponse::create($data, $httpCode);
    }
}
