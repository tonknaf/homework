<?php

namespace AppBundle\DTO;

use AppBundle\DTO\Test as TestDTO;
use AppBundle\Entity\Test;
use AppBundle\Entity\Word;

class TestDTOConverter {

    public function convert(Test $test) {

        return new TestDTO($test->getId(), $test->getStatus()->toNative(), $test->getScore(), $test->getName(),
            $test->getMistakesAvailable(), $test->hasUnansweredQuestion() ? $test->getQuestion()->getWord() : null,
            $test->hasUnansweredQuestion() ? array_map(function (Word $word) use ($test) {

                return new Answer($word->getId(), $word->getWord(), $test->getIsFailedAnswer($word));
            }, $test->getAnswers()->toArray()) : null);
    }
}