<?php

namespace AppBundle\Application;

use AppBundle\Entity as DomainExceptions;
use AppBundle\Entity\Test;
use AppBundle\Entity\Word;
use AppBundle\Repository\TestRepository;
use AppBundle\Repository\TranslationRepository;
use AppBundle\Repository\WordRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;

class AskNewQuestion {

    /**
     * @var TestRepository
     */
    private $testRepository;

    /**
     * @var WordRepository
     */
    private $wordRepository;

    /**
     * @var TranslationRepository
     */
    private $translationRepository;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(
        TestRepository $testRepository,
        WordRepository $wordRepository,
        TranslationRepository $translationRepository,
        EntityManager $em
    ) {

        $this->testRepository = $testRepository;
        $this->wordRepository = $wordRepository;
        $this->translationRepository = $translationRepository;
        $this->em = $em;
    }

    /**
     * @param int $id
     *
     * @throws DomainExceptions\TestIsEndedException
     * @throws DomainExceptions\UnansweredQuestionException
     * @throws DomainExceptions\UsedWordException
     * @throws TestIsEndedException
     * @throws TestNotFoundException
     * @throws UnansweredQuestionException
     * @throws \Exception
     */
    public function execute(int $id) {

        /**
         * @var Test $test
         */
        $test = $this->testRepository->find($id);

        if (!$test) {
            throw new TestNotFoundException;
        }

        if ($test->isEnded()) {
            throw new TestIsEndedException;
        }

        if ($test->hasUnansweredQuestion()) {
            throw new UnansweredQuestionException;
        }

        $usedWordIds = $test->getUsedWords()->map(function (Word $word) {

            return $word->getId();
        });

        try {
            $question = $this->wordRepository->findRandomWord($usedWordIds->toArray());
        } catch (NoResultException $e) {
            if (count($usedWordIds) === 0) {
                throw new \Exception('No words.');
            } else {
                $test->end();
                $this->em->persist($test);
                $this->em->flush();
                throw new TestIsEndedException;
            }
        }

        try {
            $translation = $this->translationRepository->findTranslationForWord($question->getId());
        } catch (NoResultException $e) {
            throw new \Exception(sprintf('Translation not found for Word #%d.', $question->getId()));
        }

        $rightAnswer = $translation->getTranslation($question);

        $wrongAnswers = $this->wordRepository->findRandomWord([$rightAnswer->getId()], $rightAnswer->getLang(), 3);

        if (count($wrongAnswers) !== 3) {
            throw new \Exception(sprintf('Not enough wrong words for answer #%d.', $rightAnswer->getId()));
        }

        $test->askNewQuestion($question, $rightAnswer, $wrongAnswers);

        $this->em->persist($test);
        $this->em->flush($test);
    }
}