<?php

namespace AppBundle\Application;

use AppBundle\DTO\Test as TestDTO;
use AppBundle\DTO\TestDTOConverter;
use AppBundle\Entity as DomainExceptions;
use AppBundle\Entity\Test;
use AppBundle\Repository\TestRepository;

class GetTest {

    /**
     * @var TestRepository
     */
    private $repository;

    /**
     * @var TestDTOConverter
     */
    private $converter;

    public function __construct(TestRepository $repository, TestDTOConverter $converter) {

        $this->repository = $repository;
        $this->converter = $converter;
    }

    /**
     * @param int $id
     *
     * @return TestDTO|null
     * @throws TestNotFoundException
     */
    public function execute(int $id) {

        /**
         * @var Test $test
         */
        $test = $this->repository->find($id);

        if (!$test) {
            throw new TestNotFoundException;
        }

        return $this->converter->convert($test);
    }
}