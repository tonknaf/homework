<?php

namespace AppBundle\Application;

use App\Application\RepeatingAnswerException;
use AppBundle\Entity as DomainExceptions;
use AppBundle\Entity\Test;
use AppBundle\Entity\Word;
use AppBundle\Repository\TestRepository;
use AppBundle\Repository\WordRepository;
use Doctrine\ORM\EntityManager;

class AnswerQuestion {

    /**
     * @var TestRepository
     */
    private $testRepository;

    /**
     * @var WordRepository
     */
    private $wordRepository;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(TestRepository $testRepository, WordRepository $wordRepository, EntityManager $em) {

        $this->testRepository = $testRepository;
        $this->wordRepository = $wordRepository;
        $this->em = $em;
    }


    /**
     * @param $testId
     * @param $wordId
     *
     * @throws NoQuestionException
     * @throws RepeatingAnswerException
     * @throws RightAnswerException
     * @throws TestIsEndedException
     * @throws TestNotFoundException
     * @throws WordNotFoundException
     * @throws WrongAnswerException
     */
    public function execute($testId, $wordId) {

        /**
         * @var Test $test
         */
        $test = $this->testRepository->find($testId);

        if (!$test) {
            throw new TestNotFoundException;
        }

        /**
         * @var Word $word
         */
        $word = $this->wordRepository->find($wordId);

        if (!$word) {
            throw new WordNotFoundException;
        }

        try {
            $test->answer($word);
        } catch (DomainExceptions\RightAnswerException $e) {
            $this->em->persist($test);
            $this->em->flush();
            throw new RightAnswerException;
        } catch (DomainExceptions\WrongAnswerException $e) {
            $this->em->persist($test);
            $this->em->flush();
            if ($test->isEnded()) {
                throw new TestIsEndedException;
            } else {
                throw new WrongAnswerException;
            }
        } catch (DomainExceptions\TestIsEndedException $e) {
            throw new TestIsEndedException;
        } catch (DomainExceptions\NoQuestionException $e) {
            throw new NoQuestionException;
        } catch (DomainExceptions\RepeatingAnswerException $e) {
            throw new RepeatingAnswerException;
        } catch (DomainExceptions\AnswerDoesNotBelongToTheQuestionException $e) {
            throw new WordNotFoundException;
        }
    }
}