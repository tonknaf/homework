<?php

namespace AppBundle\Application;

use AppBundle\Entity as DomainExceptions;
use AppBundle\Entity\Test;
use AppBundle\Value\Username;
use Doctrine\ORM\EntityManager;

class BeginTest {

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em) {

        $this->em = $em;
    }

    /**
     * @param string $name
     *
     * @return int
     * @throws InvalidUsernameException
     */
    public function execute(string $name) : int {

        try {
            $username = new Username($name);
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUsernameException;
        }

        $test = Test::begin($username);

        $this->em->persist($test);
        $this->em->flush();

        return $test->getId();
    }
}