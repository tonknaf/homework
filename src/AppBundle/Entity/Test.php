<?php

namespace AppBundle\Entity;

use AppBundle\Value\Username;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Test
 *
 * @ORM\Table(name="test")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TestRepository")
 */
class Test {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TestStatus
     *
     * @ORM\Embedded(class="AppBundle\Entity\TestStatus")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Question", mappedBy="test", cascade={"persist", "remove"})
     */
    private $questions;

    /**
     * @var Question
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Question", cascade={"persist", "remove"})
     */
    private $unansweredQuestion;

    /**
     * @var integer
     *
     * @ORM\Column(name="lives", type="smallint")
     */
    private $mistakesAvailable;

    public function __construct() {

        $this->questions = new ArrayCollection();
    }

    /**
     * @param Username $name
     *
     * @return Test
     * @throws UnansweredQuestionException
     * @throws TestIsEndedException
     * @throws UsedWordException
     */
    public static function begin(Username $name) : Test {

        $t = new Test();

        $t->name = $name;
        $t->score = 0;
        $t->mistakesAvailable = 3;
        $t->status = TestStatus::NoQuestion();

        return $t;
    }

    /**
     * Get id
     * @codeCoverageIgnore
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * @deprecated Only for testing purposes
     */
    public function _setId($id) {

        $this->id = $id;
    }

    /**
     * @return TestStatus
     */
    public function getStatus() : TestStatus {

        return $this->status;
    }

    /**
     * @return int
     */
    public function getScore() : int {

        return $this->score;
    }

    /**
     * @return int
     */
    public function getMistakesAvailable() : int {

        return $this->mistakesAvailable;
    }

    /**
     * @return string
     */
    public function getName() : string {

        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions() {

        return $this->questions;
    }

    /**
     * @return Word
     */
    public function getQuestion() {

        $this->shouldHaveUnansweredQuestion();

        return $this->unansweredQuestion->getQuestion();
    }

    private function shouldHaveUnansweredQuestion() {

        if (!$this->hasUnansweredQuestion()) {
            throw new NoQuestionException;
        }
    }

    /**
     * @return bool
     */
    public function hasUnansweredQuestion() : bool {

        return $this->status->equalTo(TestStatus::UnansweredQuestion());
    }

    public function getIsFailedAnswer(Word $word) {

        $this->shouldHaveUnansweredQuestion();

        return $this->unansweredQuestion->isFailedAnswer($word);
    }

    public function getAnswers() {

        $this->shouldHaveUnansweredQuestion();

        return $this->unansweredQuestion->getAnswers();
    }

    /**
     * @return bool
     */
    public function isEnded() : bool {

        return $this->status->equalTo(TestStatus::Ended());
    }

    /**
     * @return ArrayCollection
     */
    public function getUsedWords() {

        return $this->questions->map(function (Question $r) {

            return $r->getQuestion();
        });
    }

    /**
     * @param Word $question
     * @param Word $rightAnswer
     * @param array $wrongAnswers
     *
     * @return $this
     * @throws UnansweredQuestionException
     * @throws TestIsEndedException
     * @throws UsedWordException
     */
    public function askNewQuestion(Word $question, Word $rightAnswer, array $wrongAnswers) {

        if ($this->isEnded()) {
            throw new TestIsEndedException;
        }

        $this->shouldNotHaveUnansweredQuestion();

        if ($this->getUsedWords()->contains($question)) {
            throw new UsedWordException;
        }

        $this->setQuestion(Question::ask($this, $question, $rightAnswer, $wrongAnswers));

        return $this;
    }

    /**
     * @param Word $word
     *
     * @return $this
     * @throws AnswerDoesNotBelongToTheQuestionException
     * @throws NoQuestionException
     * @throws RepeatingAnswerException
     * @throws RightAnswerException
     * @throws TestIsEndedException
     * @throws WrongAnswerException
     */
    public function answer(Word $word) {

        $this->shouldHaveUnansweredQuestion();

        try {
            $this->unansweredQuestion->answer($word);
        } catch (RightAnswerException $e) {
            $this->score++;
            $this->removeQuestion();
            throw $e;
        } catch (WrongAnswerException $e) {
            $this->mistakeHappened();
            throw $e;
        }

        return $this;
    }

    /**
     * @throws UnansweredQuestionException
     */
    private function shouldNotHaveUnansweredQuestion() {

        if ($this->hasUnansweredQuestion()) {
            throw new UnansweredQuestionException;
        }
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    private function setQuestion(Question $question) {

        $this->questions->add($this->unansweredQuestion = $question);

        $this->status = TestStatus::UnansweredQuestion();

        return $this;
    }

    /**
     * @return $this
     */
    private function removeQuestion() {

        $this->unansweredQuestion = null;
        $this->status = TestStatus::NoQuestion();

        return $this;
    }

    private function mistakeHappened() {

        $this->mistakesAvailable--;
        if ($this->mistakesAvailable <= 0) {
            $this->removeQuestion();
            $this->end();
            throw new TestIsEndedException;
        }

        return $this;
    }

    /**
     * @return $this
     * @throws UnansweredQuestionException
     */
    public function end() {

        $this->removeQuestion();
        $this->status = TestStatus::Ended();
        return $this;
    }
}

